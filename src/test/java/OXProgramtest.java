/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.swing.text.html.HTML;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ทักช์ติโชค
 */
public class OXProgramtest {

    @Test
    public void testcheckVerticePlayerOCol1Win() {

        char table[][] = {{'O', '-', '-'},
                                      {'O', '-', '-'},
                                      {'O', '-', '-'}};
        char currentPlayer = 'O';
        int row, col = 1;

        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));

    }

    @Test
    public void testcheckVerticePlayerOCol2Win() {

        char table[][] = {{'-', 'O', '-'},
                                      {'-', 'O', '-'},
                                      {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int row, col = 2;

        assertEquals(
                true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testcheckVerticePlayerOCol3Win() {

        char table[][] = {{'-', '-', 'O'},
                                      {'-', '-', 'O'},
                                      {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int row, col = 3;

        assertEquals(
                true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testcheckVerticePlayerXCol1Win() {

        char table[][] = {{'X', '-', '-'}, 
                                      {'X', '-', '-'}, 
                                      {'X', '-', '-'}};
        char currentPlayer = 'X';
        int row, col=1;

        assertEquals(
                true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
     @Test
    public void testcheckVerticePlayerXCol2Win() {

        char table[][] = {{'-', 'X', '-'}, 
                                      {'-', 'X', '-'}, 
                                      {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int row, col=2;

        assertEquals(
                true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
             @Test
    public void testcheckVerticePlayerXCol3Win() {

        char table[][] = {{'-', '-', 'X'}, 
                                      {'-', '-', 'X'}, 
                                      {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int row, col=3;

        assertEquals(
                true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
        @Test
    public void testcheckHorizontalPlayerORow1Win() {

        char table[][] = {{'O', 'O', 'O'},
                                      {'-', '-', '-'},
                                      {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1, col;

        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));

    }
    
        @Test
    public void testcheckHorizontalPlayerORow2Win() {

        char table[][] = {{'-', '-', '-'},
                                      {'O', 'O', 'O'},
                                      {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2, col;

        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testcheckHorizontalPlayerORow3Win() {

        char table[][] = {{'-', '-', '-'},
                                      {'-', '-', '-'},
                                      {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3, col;

        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
        @Test
    public void  testcheckHorizontalPlayerXRow1Win() {

        char table[][] = {{'X', 'X', 'X'}, 
                                      {'-', '-', '-'}, 
                                      {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1, col;

        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
     @Test
    public void  testcheckHorizontalPlayerXRow2Win() {

        char table[][] = {{'-', '-', '-'}, 
                                      {'X', 'X', 'X'}, 
                                      {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2, col;

        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
             @Test
    public void  testcheckHorizontalPlayerXRow3Win() {

        char table[][] = {{'-', '-', '-'}, 
                                      {'-', '-', '-'}, 
                                      {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3, col;

        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

        @Test
        public void  testcheckOI1PlayerOWin() {

        char table[][] = {{'-', '-', 'O'}, 
                                      {'-', 'O', '-'}, 
                                      {'O', '-', '-'}};
        char currentPlayer = 'O';

        assertEquals(true, OXProgram.checkX(table, currentPlayer));
    }
    
        @Test
        public void  testcheckO2PlayerOWin() {

        char table[][] = {{'O', '-', '-'}, 
                                      {'-', 'O', '-'}, 
                                      {'-', '-', 'O'}};
        char currentPlayer = 'O';

        assertEquals(true, OXProgram.checkX(table, currentPlayer));
    }
        @Test
        public void  testcheckX1PlayerXWin() {

        char table[][] = {{'-', '-', 'X'}, 
                                      {'-', 'X', '-'}, 
                                      {'X', '-', '-'}};
        char currentPlayer = 'X';

        assertEquals(true, OXProgram.checkX(table, currentPlayer));
    }
    
        @Test
        public void  testcheckX2PlayerXWin() {

        char table[][] = {{'X', '-', '-'}, 
                                      {'-', 'X', '-'}, 
                                      {'-', '-', 'X'}};
        char currentPlayer = 'X';

        assertEquals(true, OXProgram.checkX(table, currentPlayer));
    } 
        
                @Test
        public void  testcheckDraw() {
            
        int count = 8;
           
        assertEquals(true, OXProgram.checkDraw(count));
    } 
}
